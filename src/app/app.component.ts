import { Component } from '@angular/core';
import { Observable, interval } from 'rxjs';
import html2canvas from 'html2canvas';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'project';
  folder: any;
  canvas = document.createElement("canvas");
  ctx = this.canvas.getContext("2d");
  imageData;
  sub;
  resultImage;
  totalImagesRendered: number = 0;
  imageParts = [];
  tileImages = [];
  splitUpImage = [];
  DelteEdistances = [];
  tilesWithSmallestDistance = [];
  img = new Image();
  outputImg = new Image();
  rendering: boolean = false;
  hasLoaded: boolean = false;

  async renderImage(imgFile, imagesFolder) {
    //Initializing variables 
    this.imageParts = [];
    this.tileImages = [];
    this.splitUpImage = [];
    this.DelteEdistances = [];
    this.tilesWithSmallestDistance = [];
    this.hasLoaded = false;
    this.rendering = true;

    //1. Calculate avg RGB for each tile image (Avg R, Avg G, Avg B)
    var count: number = 0;
    this.sub = interval(200)
      .subscribe((val) => {
        var reader = new FileReader();
        reader.onload = () => this.displayTileImages(reader);
        reader.readAsDataURL(imagesFolder[count]);
        count++;
        if (count >= imagesFolder.length) {
          debugger;
          this.sub.unsubscribe();
          //2. Divide our input image in 20x20 parts. (You can change this however you like)
          this.splitSelectedImage(imgFile);
        }
      });
  }

  displayTileImages(fileReader) {
    var img = document.getElementById("tileImage") as HTMLImageElement;
    img.onload = () => this.getImageData(img, true);
    img.src = fileReader.result;
  }

  getImageData(img, isTileImages) {
    this.ctx.drawImage(img, 0, 0);
    this.imageData = this.ctx.getImageData(0, 0, img.width, img.height);
    var red: number = 0;
    var green: number = 0;
    var blue: number = 0;
    var pixels: number = 0;
    var alpha: number = 0;

    for (var i = 0; i < this.imageData.data.length; i += 4) {
      red += this.imageData.data[i];
      green += this.imageData.data[i + 1];
      blue += this.imageData.data[i + 2];
      alpha += this.imageData.data[i + 3];
      pixels += 1;
    }

    var avgRed = (red / pixels);
    var avgGreen = (green / pixels);
    var avgBlue = (blue / pixels);
    var totalAvgRgb = ((red / pixels) + (green / pixels) + (blue / pixels) / 3);

    this.totalImagesRendered += 1;
    console.log("Avg Red: " + avgRed + " Avg Green: " + avgGreen + " Avg Blue: " + avgBlue + " Total RGB AVG: " + totalAvgRgb + " Pixels: " + pixels);
    console.log("Total Images Rendered: " + this.totalImagesRendered);

    if (isTileImages) {
      this.tileImages.push({
        avgRed: avgRed,
        avgGreen: avgGreen,
        avgBlue: avgBlue,
        totalAvgRgb: totalAvgRgb,
        pixels: pixels,
        src: img.src,
        viewImage: false
      });
    } else {
      this.splitUpImage.push({
        avgRed: avgRed,
        avgGreen: avgGreen,
        avgBlue: avgBlue,
        pixels: pixels,
        src: img.src,
        viewImage: false
      });
    }
  }

  splitSelectedImage(imgFile) {
    var reader = new FileReader();
    reader.onload = () => this.displaySelectedImage(reader);
    reader.readAsDataURL(imgFile[0]);
  }

  displaySelectedImage(fileReader) {
    this.img = document.getElementById("DividedImage") as HTMLImageElement;
    // 3. Calculate the avg RGB for each of the 400 parts in our input image.
    this.img.onload = () => this.splitImage20x20();
    this.img.src = fileReader.result;
  }

  splitImage20x20() {
    var widthx20 = this.img.width / 20;
    var heightx20 = this.img.height / 20;
    var count = 0;
    for (let y = 0; y < 20; y++) {
      for (let x = 0; x < 20; x++) {
        this.canvas.width = widthx20;
        this.canvas.height = heightx20;
        this.ctx.drawImage(this.img, x * widthx20, y * heightx20, widthx20, heightx20, 0, 0, widthx20, heightx20);
        this.imageParts.push(this.canvas.toDataURL());
        var slicedImage = document.createElement('img');
        slicedImage.src = this.imageParts[count];
        slicedImage.width = widthx20;
        slicedImage.height = heightx20;
        slicedImage.style.paddingRight = "10px";
        count++;
        this.getImageData(slicedImage, false);
        if (count >= 400) {
          //4. Calculate the distance between every tile (AVG RGB) and every part of our image (AVG RGB)
          this.calculateAvgRgbImageAndFolder();
        }
        var div = document.getElementById('splitImage');
        div.style.width = "90%";
        div.style.marginLeft = "5%";
        div.appendChild(slicedImage);
      }
    }
    console.log(this.imageParts);
  }



  calculateAvgRgbImageAndFolder() {
    var position = 0;
    if (this.tileImages.length <= this.splitUpImage.length) {
      for (let index = 0; index < this.tileImages.length; index++) {
        var rgbA = [this.tileImages[index].avgRed, this.tileImages[index].avgGreen, this.tileImages[index].avgBlue];
        var rgbB = [this.splitUpImage[index].avgRed, this.splitUpImage[index].avgGreen, this.splitUpImage[index].avgBlue];
        this.DelteEdistances.push({
          distance: this.deltaE(rgbA, rgbB),
          image1Data: this.tileImages[index],
          image2Data: this.splitUpImage[index],
          position: position
        });
        position++;
      }
    } else {
      for (let index = 0; index < this.splitUpImage.length; index++) {
        var rgbA = [this.tileImages[index].avgRed, this.tileImages[index].avgGreen, this.tileImages[index].avgBlue];
        var rgbB = [this.splitUpImage[index].avgRed, this.splitUpImage[index].avgGreen, this.splitUpImage[index].avgBlue];
        this.DelteEdistances.push({
          distance: this.deltaE(rgbA, rgbB),
          imageData: this.tileImages[index],
          position: position
        });
        position++;
      }
    }

    console.log(this.DelteEdistances);

    // 5. Choose the tiles with the smallest distance, resize them and replace that image part with the tile
    var minDistance = this.DelteEdistances[0].distance;

    for (let index = 0; index < this.DelteEdistances.length; index++) {
      if (this.DelteEdistances[index].distance < minDistance) {
        minDistance = this.DelteEdistances[index].distance;
      }
    }

    for (let index = 0; index < this.DelteEdistances.length; index++) {
      if (this.DelteEdistances[index].distance == minDistance) {
        this.tilesWithSmallestDistance.push(this.DelteEdistances[index]);
      }
    }

    this.outputImg.src = this.tilesWithSmallestDistance[0].image1Data.src;
    var widthx10 = this.img.width / 20;
    var heightx10 = this.img.height / 20;
    this.outputImg.height = widthx10;
    this.outputImg.width = heightx10;
    this.splitUpImage[position].src = this.outputImg.src;
    var canvas: any = document.getElementById('canvas');
    var context = canvas.getContext('2d');
    canvas.width = this.img.width;
    canvas.height = this.img.height;
    var div = document.getElementById('outputImage');
    div.style.width = (this.outputImg.width * 20) + "px"
    div.style.lineHeight = "0px";
    div.style.backgroundColor = "black"

    for (let index = 0; index < this.splitUpImage.length; index++) {
      var slicedImage = document.createElement('img');
      slicedImage.src = this.splitUpImage[index].src;
      slicedImage.width = this.outputImg.width;
      slicedImage.height = this.outputImg.height;
      div.appendChild(slicedImage);
    }

    // 6. Save output image
    html2canvas(document.querySelector("#outputImage")).then(canvas => {
      document.body.appendChild(canvas)
      this.resultImage = canvas.toDataURL("image/png").replace("image/png", "image/octet-stream");
      console.log(this.resultImage);
      window.location.href = this.resultImage;
      canvas.remove();
      this.rendering = false;
      this.hasLoaded = true;
      debugger;
    });
    console.log("Min Distance: " + minDistance);
    console.log("Smallest distance array: " + this.tilesWithSmallestDistance);

    debugger;

  }

  filesPicked(files) {
    for (let i = 0; i < files.length; i++) {
      const file = files[i];
      const path = file.webkitRelativePath.split('/');
    }
  }

  rgb2lab(rgb) {
    let r = rgb[0] / 255, g = rgb[1] / 255, b = rgb[2] / 255, x, y, z;
    r = (r > 0.04045) ? Math.pow((r + 0.055) / 1.055, 2.4) : r / 12.92;
    g = (g > 0.04045) ? Math.pow((g + 0.055) / 1.055, 2.4) : g / 12.92;
    b = (b > 0.04045) ? Math.pow((b + 0.055) / 1.055, 2.4) : b / 12.92;
    x = (r * 0.4124 + g * 0.3576 + b * 0.1805) / 0.95047;
    y = (r * 0.2126 + g * 0.7152 + b * 0.0722) / 1.00000;
    z = (r * 0.0193 + g * 0.1192 + b * 0.9505) / 1.08883;
    x = (x > 0.008856) ? Math.pow(x, 1 / 3) : (7.787 * x) + 16 / 116;
    y = (y > 0.008856) ? Math.pow(y, 1 / 3) : (7.787 * y) + 16 / 116;
    z = (z > 0.008856) ? Math.pow(z, 1 / 3) : (7.787 * z) + 16 / 116;
    return [(116 * y) - 16, 500 * (x - y), 200 * (y - z)]
  }


  deltaE(rgbA, rgbB) {
    let labA = this.rgb2lab(rgbA);
    let labB = this.rgb2lab(rgbB);
    let deltaL = labA[0] - labB[0];
    let deltaA = labA[1] - labB[1];
    let deltaB = labA[2] - labB[2];
    let c1 = Math.sqrt(labA[1] * labA[1] + labA[2] * labA[2]);
    let c2 = Math.sqrt(labB[1] * labB[1] + labB[2] * labB[2]);
    let deltaC = c1 - c2;
    let deltaH = deltaA * deltaA + deltaB * deltaB - deltaC * deltaC;
    deltaH = deltaH < 0 ? 0 : Math.sqrt(deltaH);
    let sc = 1.0 + 0.045 * c1;
    let sh = 1.0 + 0.015 * c1;
    let deltaLKlsl = deltaL / (1.0);
    let deltaCkcsc = deltaC / (sc);
    let deltaHkhsh = deltaH / (sh);
    let i = deltaLKlsl * deltaLKlsl + deltaCkcsc * deltaCkcsc + deltaHkhsh * deltaHkhsh;
    return i < 0 ? 0 : Math.sqrt(i);
  }

  viewImage(ImageSrc) {
    ImageSrc.viewImage = true;
  }


}


